export const calculateBCWPCongDong = ( dataState: any[]) => {
  const result = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  for (let index = 0; index < dataState.length; index++) {
    const { percent } = dataState[index];
    for (let j = 0; j < percent.length; j++) {
      result[j] += percent[j][1];
    }
  }
  var x = result.filter((e) => e !== 0)
  for (let i =1; i<x.length; i++) {
    x[i] += x[i-1];
  }
  // return result;
  return x;

};

